﻿using Lab3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3
{
    public static class StudentExtension
    {
        public static double leftToComplete(this Student student)
        {
            return 100.0 - student.Progress;
        }
    }
}
