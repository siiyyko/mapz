﻿using Lab3.CourseWorkExtension;
using Lab3.Subjects;
using System.Data.Common;

namespace Lab3
{
    public class Program
    {
        static void Main(string[] args)
        {
            //MockingClass.PrintStudents();

            //var actual = MockingClass.works.OrderBy(supervisor => supervisor.Difficulty).ThenBy(supervisor => supervisor.SupervisorName).ToList();

            //var actual = MockingClass.works.GroupBy(w => w.Difficulty)
            //    .Select(group => new
            //    {
            //        Difficulty = group.Key,
            //        NameAndSubject = group.Select(g => string.Join(" - ", g.SupervisorName, g.Subject).ToArray())
            //    });

            //Console.WriteLine(actual);
            //foreach (var elem in actual)
            //{
            //    //Console.WriteLine(elem.Print());
            //    //Console.WriteLine($"Difficulty: {elem.Difficulty}");
            //    //foreach(var namesubj in elem.NameAndSubject)
            //    //{
            //    //    Console.WriteLine(namesubj);
            //    //}

            //    Console.WriteLine($"Difficulty : {elem.Difficulty}");
            //    Console.WriteLine($"Names : {string.Join(", ", elem.Name)}");
            //    Console.WriteLine();
            //}

            var actual = MockingClass.students.GroupBy(s => s.CourseWorkSubject)
    .Select(g => g.OrderBy(s => s.Progress))
    .ToArray();

            var more = MockingClass.students.MaxBy(x => x.Progress * 2);
            Console.WriteLine(more);
            Console.WriteLine(actual[1].Count());

        }
    }
}
