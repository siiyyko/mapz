﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Subjects;

namespace Lab3
{
    public class Student
    {
        public string Name { get; init; }
        public Subject CourseWorkSubject { get; set; }

        private double _progress;
        public double Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                if (value == 100) IsCompleted = true;
            }
        }
        public bool IsCompleted { get; private set; }
        private const double _profitPerHour = 2.5;

        public void studyOnCourseWork(double hours)
        {
            if (hours < 0) throw new ArgumentOutOfRangeException("Hours must be a positive number!");

            double newProgress = hours * _profitPerHour + Progress;
            Progress = newProgress < 100 ? newProgress : 100;
        }

        public Student(string name, Subject courseWorkSubject, double progress)
        {
            if (progress < 0 || progress > 100) throw new ArgumentOutOfRangeException("Progress must be a positive number in range 0-100!");

            Name = name;
            CourseWorkSubject = courseWorkSubject;
            IsCompleted = false;
            Progress = progress > 100.0 ? 100.0 : progress;
        }
    }
}
