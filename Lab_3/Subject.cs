﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Subjects
{
    public enum Subject
    {
        ObjectOrientedProgramming,
        NumericalAnalysis,
        DataBases,
        SoftwareModelling,
    }
}
