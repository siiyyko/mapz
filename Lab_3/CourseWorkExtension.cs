﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.CourseWorkExtension
{
    public static class CourseWorkExtension
    {
        public static string Print(this CourseWork work)
        {
            if (work == null)
            {
                throw new ArgumentNullException("Argument can't be null");
            }
            return String.Format("{0, -25} {1, -25} {2, -15}", work.Subject, work.SupervisorName, work.Difficulty);
        }
    }
}
