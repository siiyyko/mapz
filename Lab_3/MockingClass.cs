﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Subjects;
using Lab3.Difficulties;
using Lab3.CourseWorkExtension;
using System.Globalization;

namespace Lab3
{
    public class MockingClass
    {
        public static List<CourseWork> works = new List<CourseWork>()
        {
            new CourseWork(Subject.DataBases, Difficulty.Complicated, "Tetiana Koroteieva"),
            new CourseWork(Subject.DataBases, Difficulty.EasyPeasy, "Yevhenia Levus"),
            new CourseWork(Subject.DataBases, Difficulty.EasyPeasy, "Svitlana Yatsyshyn"),
            new CourseWork(Subject.ObjectOrientedProgramming, Difficulty.ItsOver, "Tetiana Koroteieva"),
            new CourseWork(Subject.ObjectOrientedProgramming, Difficulty.EasyPeasy, "Olha Terendii"),
            new CourseWork(Subject.ObjectOrientedProgramming, Difficulty.Intimidating, "Pavlo Kurapov"),
            new CourseWork(Subject.NumericalAnalysis, Difficulty.Thoughtful, "Nataliia Melnyk"),
            new CourseWork(Subject.NumericalAnalysis, Difficulty.Intimidating, "Tetiana Koroteieva"),
            new CourseWork(Subject.NumericalAnalysis, Difficulty.Thoughtful, "Lubov Zhuravhak"),
            new CourseWork(Subject.SoftwareModelling, Difficulty.Thoughtful, "Pavlo Serdiuk"),
            new CourseWork(Subject.SoftwareModelling, Difficulty.ItsOver, "Mykola Milkovych"),
            new CourseWork(Subject.SoftwareModelling, Difficulty.Intimidating, "Yevhenia Levus")
        };

        public static List<Student> students = new List<Student>()
        {
            new Student("Oleksii Blyzniuk", Subject.ObjectOrientedProgramming, 100),
            new Student("Volodymyr Zhyla", Subject.NumericalAnalysis, 49),
            new Student("Nataliia Kaminska", Subject.NumericalAnalysis, 96),
            new Student("Mykyta Zakharov", Subject.SoftwareModelling, 51),
            new Student("Oleksii Blyzniuk", Subject.SoftwareModelling, 64),
            new Student("Khrystyna Hrytsai", Subject.DataBases, 100),
            new Student("Viktor Shamatiienko", Subject.ObjectOrientedProgramming, 0),
            new Student("Kateryna Doskach", Subject.ObjectOrientedProgramming, 34.14),
            new Student("Kateryna Doskach", Subject.DataBases, 81),
            new Student("Kateryna Doskach", Subject.SoftwareModelling, 57.5),
        };

        public static void PrintStudents()
        {
            Console.WriteLine("{0, -20} {1, -30} {2,-95} {3, -6}\n", "Name", "Subject", "Possible supervisors - Difficulty", "Progress");

            students.Select(student => new
            {
                stud = student,
                supervisorsAndDifficulties = string.Join(", ", works.Where(coursework => coursework.Subject == student.CourseWorkSubject)
                .Select(c => string.Join(" - ", c.SupervisorName, c.Difficulty)))
            })
            .ToList()
            .ForEach(el => Console.WriteLine("{0, -20} {1, -30} {2, -95} {3, -6}", el.stud.Name, el.stud.CourseWorkSubject, el.supervisorsAndDifficulties, el.stud.Progress));

            Console.WriteLine();
            foreach(CourseWork work in works)
            {
                Console.WriteLine(work.Print());
            }
        }

    }


}

