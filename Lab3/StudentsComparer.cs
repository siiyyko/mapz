﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public class StudentsComparer : IComparer<Student>
    {
        public int Compare(Student? student1, Student? student2)
        {
            if(student1 == null || student2 == null) throw new ArgumentNullException("Neither student can be null!");
            if (student1.Progress > student2.Progress) return 1;
            else if(student2.Progress < student2.Progress) return -1;
            else return 0;
        }
    }
}
