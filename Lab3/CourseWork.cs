﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Subjects;
using Lab3.Difficulties;

namespace Lab3
{
    public class CourseWork
    {
        public Subject Subject { get; init; }
        public Difficulty Difficulty { get; set; }
        public string SupervisorName { get; init; }

        public CourseWork(Subject subject, Difficulty difficulty, string supervisorName)
        {
            Subject = subject;
            Difficulty = difficulty;
            SupervisorName = supervisorName;
        }
    }
}
