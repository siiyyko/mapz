﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Difficulties
{
    public enum Difficulty
    {
        EasyPeasy,
        Thoughtful,
        Intimidating,
        Complicated,
        ItsOver
    }
}
