﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    [TestClass]
    public class ClassTester
    {
        [TestMethod]
        public void TestSelect()
        {
            //var expected = new List<string>()
            //{
            //    "100", "49", "96", "51", "64", "100", "0", "34.14", "81", "57.5"
            //};

            var expected = new List<double>()
            {
                100, 49, 96, 51, 64, 100, 0, 34.14, 81, 57.5
            };

            var actual = MockingClass.students.Select(stud => stud.Progress).ToList();
            
            CollectionAssert.AreEqual(expected, actual, "Failed at Select test (lists are not equal)");
        }
    }
}
