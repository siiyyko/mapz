﻿using Lab4.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Levels
{
    public class BossLevel(Boss boss) : ILevel
    {
        private Boss _boss = boss;

        public void Show()
        {
            _boss.Show();
        }
    }
}
