﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Lab4.MapObjects;

namespace Lab4.Levels
{
    public interface ILevel
    {
        void Show();
    }

    public class BaseLevel : ILevel
    {

        protected List<IMapObject> _mapObjects { get; init; }

        public BaseLevel(List<IMapObject> mapObjects)
        {
            _mapObjects = mapObjects;
        }

        public void Show()
        {
            foreach (var mapObject in _mapObjects)
            {
                mapObject.Show();
            }
        }
    }

    public class TeacherDecorator : BaseLevel
    {
        protected readonly ILevel _level;

        public TeacherDecorator(ILevel level, List<IMapObject> mapObjects, List<TeacherUnit> teachers) : base(mapObjects)
        {
            _level = level;
            _mapObjects.AddRange(teachers);
        }
    }

    public class RestDecorator : BaseLevel
    {
        protected readonly ILevel _level;

        public RestDecorator(ILevel level, List<IMapObject> mapObjects, List<Rest> rests) : base(mapObjects)
        {
            _level = level;
            _mapObjects.AddRange(rests);
        }
    }

    public class DeadlineDecorator : BaseLevel
    {
        protected readonly ILevel _level;

        public DeadlineDecorator(ILevel level, List<IMapObject> mapObjects, List<IMapObject> deadlines) : base(mapObjects)
        {
            _level = level;
            _mapObjects.AddRange(deadlines);
        }
    }
}
