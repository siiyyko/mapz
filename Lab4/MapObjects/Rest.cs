﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Lab4.MapObjects
{
    public class Rest : IMapObject
    {
        private readonly Image Sprite;
        private const string SpritePath = "../resources/sprites/rest.png";
        private Vector2 _position;

        private readonly int RestLowerBound = 15;
        private readonly int RestUpperBound = 35;

        public Rest()
        {
            Sprite = new Image();
            var bm = new BitmapImage(new Uri(SpritePath, UriKind.Relative));
            Sprite.Source = bm;
            SetRandomPosition(10, 10, 40);
        }

        public virtual void SetRandomPosition(int totalCellsX, int totalCellsY, int gridSize)
        {
            var rand = new Random();
            _position = new Vector2(rand.Next(totalCellsX) * gridSize, rand.Next(totalCellsY) * gridSize);
        }

        public void Show()
        {
            Canvas.SetTop(Sprite, _position[0]);
            Canvas.SetLeft(Sprite, _position[1]);
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Add(Sprite);
        }

        public void Hide()
        {
            Random randRestGenerator = new();
            Game.Player.Heal(randRestGenerator.Next(RestLowerBound, RestUpperBound));
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
        }

        public void Destroy()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
        }

        public Image GetSprite()
        {
            return Sprite;
        }

    }
}
