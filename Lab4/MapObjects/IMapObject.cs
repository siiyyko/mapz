﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab4.MapObjects
{
    public interface IMapObject
    {
        void Show();
        void Hide();
        void Destroy();
        Image GetSprite();
    }
}
