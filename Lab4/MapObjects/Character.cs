﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Lab4.CharacterInfo;
using Lab4.Utilities;

namespace Lab4.MapObjects
{
    public class Character
    {
        private Character(int HP)
        {
            Sprite = new Image();
            var bm = new BitmapImage(new Uri("../resources/sprites/character.png", UriKind.Relative));
            Sprite.Source = bm;
            this.HP = HP;
        }

        private static Character _instance;

        public static Character Instance
        {
            get
            {
                if (_instance == null) return new Character(100);
                return _instance;
            }
        }

        public UserStats Statistics { get; set; }
        public UserPortrait Portrait { get; set; }
        private readonly Image Sprite;
        private int _hp;
        public int HP
        {
            get
            {
                return _hp;
            }
            set
            {
                _hp = value > 100 ? 100 : value;
                //if (value <= 0) Game.GameOver(GameOverStateEnum.TooLowHp);
                if (value <= 0)
                {
                    Game.TransitionTo(new LowHpGameOver());
                    Game.GameOver();
                }
                MainWindow.GetMainWindowController().HPTextBlock.Text = _hp.ToString();
            }
        }

        private uint _restsInARow = 0;
        public uint RestsInARow
        {
            get
            {
                return _restsInARow;
            }
            set
            {
                _restsInARow = value;
                if (_restsInARow > 3)
                {
                    Game.TransitionTo(new TooMuchRestGameOver());
                    Game.GameOver();
                }
            }
        }

        public static Dictionary<Key, Direction> dirs = new Dictionary<Key, Direction>
        {
            { Key.W, Direction.Up},
            { Key.S, Direction.Down},
            { Key.A, Direction.Left},
            { Key.D, Direction.Right},
        };

        public void Hurt(int HP)
        {
            this.HP -= HP;
        }

        public void Heal(int HP)
        {
            this.HP += HP;
        }

        public void Show()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Add(Sprite);

            Canvas.SetTop(Sprite, 0);
            Canvas.SetLeft(Sprite, 0);

            var mn = MainWindow.GetMainWindowController();
            mn.PreviewKeyDown += Mn_PreviewKeyDown;
        }

        private void Mn_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (!dirs.ContainsKey(e.Key)) return;
            Move(dirs[e.Key], Statistics.TimeManagement);
        }

        private bool IsOutOfBounds(Direction dir, int speed)
        {
            var mw = MainWindow.GetMainWindowController();
            const int SpriteOffset = 5;
            if (
                dir == Direction.Up && Canvas.GetTop(Sprite) - speed < 0 ||
                dir == Direction.Down && Canvas.GetTop(Sprite) + Sprite.ActualHeight + speed - SpriteOffset > mw.mainGameCanvas.ActualHeight ||
                dir == Direction.Left && Canvas.GetLeft(Sprite) - speed < 0 ||
                dir == Direction.Right && Canvas.GetLeft(Sprite) + Sprite.ActualWidth + speed - SpriteOffset > mw.mainGameCanvas.ActualWidth
              )
            {
                return true;
            }

            return false;
        }

        private void Move(Direction dir, int speed)
        {
            if (IsOutOfBounds(dir, speed)) return;

            switch (dir)
            {
                case Direction.Up:
                    Canvas.SetTop(Sprite, Canvas.GetTop(Sprite) - speed);
                    break;
                case Direction.Down:
                    Canvas.SetTop(Sprite, Canvas.GetTop(Sprite) + speed);
                    break;
                case Direction.Left:
                    Canvas.SetLeft(Sprite, Canvas.GetLeft(Sprite) - speed);
                    break;
                case Direction.Right:
                    Canvas.SetLeft(Sprite, Canvas.GetLeft(Sprite) + speed);
                    break;
            }
        }

        public bool CheckCollision(IMapObject unit)
        {
            const double threshold = 20;
            bool hasCollision;

            double distX = Canvas.GetLeft(unit.GetSprite()) - Canvas.GetLeft(Sprite);
            double distY = Canvas.GetTop(unit.GetSprite()) - Canvas.GetTop(Sprite);

            if (Math.Sqrt(distX * distX + distY * distY) < threshold) hasCollision = true;
            else hasCollision = false;

            return hasCollision;
        }

        public void ApplyRomanMarchukHelp()
        {
            MessageBox.Show("З лабами Вам допоміг Роман Марчук, дай боже! " +
                "Можливо, ваше ментальне здоров'я подякує вам, та 10 прослуханих історій і 3 келихи пива час назад не повернуть...");
            Heal(30);
            Statistics.TimeManagement -= 4;
        }

        public void ApplyPovtorkaReminder()
        {
            MessageBox.Show("Чим довше ви дивитесь на повторку, тим ліпше ви помічаєте, що повторка дивиться на вас... " +
                "Ви варитесь у суміші тривоги, поспіху і чвертьсні. У такій суматосі не дивно не помітити, як деякі лаби зробилися самі. ");
            Hurt(35);
            Statistics.TimeManagement += 6;
            
        }
    }

}
