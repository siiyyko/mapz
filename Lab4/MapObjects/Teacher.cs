﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Lab4.MapObjects
{
    public abstract class TeacherUnit : IMapObject
    {
        public int Health { get; set; }
        public Vector2 Position { get; set; }
        protected string? SpritePath;
        public Image? Sprite;

        protected Random RandHpGenerator = new();
        protected int HPlowerBound;
        protected int HPupperBound;

        public abstract TeacherUnit Clone();

        public virtual void Show()
        {
            Sprite = new Image();
            var bm = new BitmapImage(new Uri(SpritePath, UriKind.Relative));
            Sprite.Source = bm;
            Canvas.SetTop(Sprite, Position[0]);
            Canvas.SetLeft(Sprite, Position[1]);
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Add(Sprite);
        }
        public virtual void Hide()
        {
            Game.Player.Hurt(Health);
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
        }

        public virtual void SetRandomPosition(int totalCellsX, int totalCellsY, int gridSize)
        {
            var rand = new Random();
            Position = new Vector2(rand.Next(totalCellsX) * gridSize, rand.Next(totalCellsY) * gridSize);
        }

        public void Destroy()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
        }

        public Image GetSprite()
        {
            return Sprite;
        }
    }

    public class EasyTeacherUnit : TeacherUnit
    {
        public EasyTeacherUnit()
        {
            HPlowerBound = 5;
            HPupperBound = 30;

            Health = RandHpGenerator.Next(HPlowerBound, HPupperBound);
            SpritePath = "../resources/sprites/unitSprites/easyTeacher.png";
            SetRandomPosition(10, 10, 40);
        }

        public override TeacherUnit Clone() => new EasyTeacherUnit { SpritePath = SpritePath };

    }

    public class MediumTeacherUnit : TeacherUnit
    {
        public MediumTeacherUnit()
        {
            HPlowerBound = 10;
            HPupperBound = 35;

            Health = RandHpGenerator.Next(HPlowerBound, HPupperBound);
            SpritePath = "../resources/sprites/unitSprites/mediumTeacher.png";
            SetRandomPosition(10, 10, 40);
        }

        public override TeacherUnit Clone() => new MediumTeacherUnit { SpritePath = SpritePath };
    }

    public class HardTeacherUnit : TeacherUnit
    {
        public HardTeacherUnit()
        {
            HPlowerBound = 20;
            HPupperBound = 35;

            Health = RandHpGenerator.Next(HPlowerBound, HPupperBound);
            SpritePath = "../resources/sprites/unitSprites/hardTeacher.png";
            SetRandomPosition(10, 10, 40);
        }

        public override TeacherUnit Clone() => new HardTeacherUnit { SpritePath = SpritePath };

    }
}
