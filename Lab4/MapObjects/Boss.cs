﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Lab4.MapObjects
{
    public abstract class Boss : IMapObject
    {
        protected Image Sprite;
        protected string SpritePath;

        public abstract void Destroy();
        public Image GetSprite() {  return Sprite; }
        public abstract void Hide();
        public abstract void Show();

        protected string[] PhrasesList;
        protected string phrasesPath;
        protected abstract string[] ReadPhrasesListFromFile();
        protected abstract Task ShowPhrase();

        protected abstract void ShowEndMessage();
    }

    public class TarasOrestovych : Boss
    {
        public TarasOrestovych()
        {
            SpritePath = "../resources/sprites/TarasOrestovych.png";
            Sprite = new Image();
            Sprite.Height = 375;
            var bm = new BitmapImage(new Uri(SpritePath, UriKind.Relative));
            Sprite.Source = bm;

            phrasesPath = "../resources/phrases/TarasOrestovych.txt";
            PhrasesList = ReadPhrasesListFromFile();

            rand = new();
            Health = 200;
        }

        private int _health;
        public int Health { get
            {
                return _health;
            }
            set
            {
                if(value < 0)
                {
                    _health = 0;
                    ShowEndMessage();
                    Hide();
                }
                else
                {
                    _health = value;
                }
            }
        }
        private readonly Random rand;

        protected override string[] ReadPhrasesListFromFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(phrasesPath, Encoding.UTF8);
                return lines;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Error reading file: {ex.Message}");
                return null;
            }
        }

        private bool IsBossAlive()
        {
            if (Health > 0) return true;
            else return false;
        }

        private int GetRandomDelay(int from, int to)
        {
            return rand.Next(from, to);
        }

        public override void Show()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Add(Sprite);
            Sprite.MouseLeftButtonDown += Hurt;
            //ShowPhrase();
            if (IsBossAlive())
            {
                DispatcherTimer timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromSeconds(GetRandomDelay(8, 20));
                timer.Tick += (sender, e) =>
                {
                    ShowPhrase();
                };
                timer.Start();
            }
        }

        public override void Hide()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
            Sprite.MouseLeftButtonDown -= Hurt;
        }

        public override void Destroy()
        {
            Hide();
        }

        private void Hurt(object sender, EventArgs e)
        {
            Health -= rand.Next(0, 30);
        }

        protected override void ShowEndMessage()
        {
            MessageBox.Show("End!");
        }

        async Task RemoveContainerAfterDelay(Grid container, double desiredDelay)
        {
            await Task.Delay(TimeSpan.FromSeconds(desiredDelay));
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(container);
        }

        protected async override Task ShowPhrase()
        {
            Grid container = new();
            container.Width = 200;
            container.Height = 50;
            container.Margin = new System.Windows.Thickness(5);

            Rectangle rect = new();
            rect.Fill = Brushes.Beige;
            container.Children.Add(rect);

            TextBox phrase = new();
            phrase.Text = PhrasesList[rand.Next(0, PhrasesList.Length)];
            container.Children.Add(phrase);

            MainWindow.GetMainWindowController().mainGameCanvas.Children.Add(container);
            //wait for ? seconds
            //then remove from canvas

            await RemoveContainerAfterDelay(container, 8);
        }

    }
}
