﻿using Lab4.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Lab4.MapObjects
{
    public class Deadline : IMapObject
    {
        private readonly Image Sprite;
        private const string SpritePath = "../resources/sprites/deadline.png";
        private Vector2 _position;


        public Deadline()
        {
            Sprite = new Image();
            var bm = new BitmapImage(new Uri(SpritePath, UriKind.Relative));
            Sprite.Source = bm;
            SetRandomPosition(10, 10, 40);
        }

        public virtual void SetRandomPosition(int totalCellsX, int totalCellsY, int gridSize)
        {
            var rand = new Random();
            _position = new Vector2(rand.Next(totalCellsX) * gridSize, rand.Next(totalCellsY) * gridSize);
        }

        public void Show()
        {
            Canvas.SetTop(Sprite, _position[0]);
            Canvas.SetLeft(Sprite, _position[1]);
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Add(Sprite);
        }

        public void Hide()
        {
            Game.TransitionTo(new DeadlineTouchedGameOver());
            Game.GameOver();
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
        }

        public Image GetSprite()
        {
            return Sprite;
        }

        public void Destroy()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Remove(Sprite);
        }
    }

    public class DeadlineProxy : IMapObject
    {
        private readonly Deadline _realDeadline;
        private readonly string _logFilePath;
        private static readonly object _lock = new object();

        public DeadlineProxy(Deadline realDeadline, string logFilePath)
        {
            _realDeadline = realDeadline;
            _logFilePath = logFilePath;
        }

        public Image GetSprite()
        {
            return _realDeadline.GetSprite();
        }

        public void Hide()
        {
            string logMessage = $"{DateTime.Now.ToString("[dd.MM.yyyy HH:mm:ss]")} Player collided with an object!";
            lock (_lock)
            {
                Console.WriteLine(logMessage);
                File.AppendAllText(_logFilePath, logMessage + Environment.NewLine);
            }
            _realDeadline.Hide();
        }

        public void Show()
        {
            string logMessage = $"{DateTime.Now.ToString("[dd.MM.yyyy HH:mm:ss]")} Object sprite has shown!";
            lock (_lock)
            {
                Console.WriteLine(logMessage);
                File.AppendAllText(_logFilePath, logMessage + Environment.NewLine);
            }
            _realDeadline.Show();
        }

        public void Destroy()
        {
            string logMessage = $"{DateTime.Now.ToString("[dd.MM.yyyy HH:mm:ss]")} Object has been destroyed!";
            lock (_lock)
            {
                Console.WriteLine(logMessage);
                File.AppendAllText(_logFilePath, logMessage + Environment.NewLine);
            }
            _realDeadline.Destroy();
        }
    }
}
