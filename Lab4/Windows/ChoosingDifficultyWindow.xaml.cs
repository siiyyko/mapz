﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab4.Utilities;

namespace Lab4
{
    public partial class ChoosingDifficultyWindow : Window
    {
        public ChoosingDifficultyWindow()
        {
            InitializeComponent();
        }

        public Difficulty chosenDifficulty { get; set; }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton radiobutton = sender as RadioButton;
            if (radiobutton == null || radiobutton.Tag == null) return;

            string tagDifficulty = radiobutton.Tag.ToString();
            
            chosenDifficulty = (Difficulty)Enum.Parse(typeof(Difficulty), tagDifficulty);
        }

        private List<RadioButton> GetRadioButtonsFromStackPanel()
        {
            var radioButtons = new List<RadioButton>();

            StackPanel sp = RadioButtonsStackPanel;
            foreach(var child in sp.Children)
                if(child is RadioButton)
                    radioButtons.Add(child as RadioButton);

            return radioButtons;
        }

        private bool isOneRadioButtonChosen(List<RadioButton> radioButtons)
        {
            foreach(var rb in radioButtons)
            {
                if (rb.IsChecked == true)
                    return true;
            }
            return false;
        }

        private void OKbuttonClick(object sender, RoutedEventArgs e)
        {
            if (!isOneRadioButtonChosen(GetRadioButtonsFromStackPanel()))
            {
                MessageBox.Show("Please, choose a difficulty!");
                return;
            }
            else
            {
                Close();
            }
        }
    }
}
