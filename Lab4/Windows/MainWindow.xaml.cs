﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab4.CharacterInfo;

namespace Lab4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Game game = new Game();
            game.StartGame();
        }

        public static MainWindow GetMainWindowController()
        {
            var mn = Application.Current.MainWindow as MainWindow ?? throw new Exception("Can't get MainWindow");
            return mn;
        }

        public static void ShowStats(UserStats stats)
        {
            var mn = GetMainWindowController();

            mn.IntellectTextBlock.Text = stats.Intellect.ToString();
            mn.HumorTextBlock.Text = stats.Humor.ToString();
            mn.SlaynessTextBlock.Text = stats.Slayness.ToString();
            mn.TimeManagementTextBlock.Text = stats.TimeManagement.ToString();
            mn.BondsTextBlock.Text = stats.Bonds.ToString();
        }

        public static void ShowPortrait(UserPortrait portrait)
        {
            var mn = GetMainWindowController();

            var bm = new BitmapImage(new Uri(portrait.GetImagePath(), UriKind.Relative));

            mn.CharacterPortrait.Source = bm;
        }
    }
}