﻿using System.Windows.Input;
using System.Windows;
using Lab4.MapObjects;
using Lab4.CharacterInfo;
using Lab4.Utilities;
using Lab4.Levels;
using System.Security.Policy;
using System.Runtime.CompilerServices;
using System.Xml.Serialization;

namespace Lab4
{
    public class Game : ISubscriber
    {
        private IDifficultyFactory Factory { get; set; }

        public static Character Player { get; set; }
        private TeacherUnit Teacher { get; set; }
        private List<IMapObject> AllUnits { get; set; }
        private BaseLevel currentLevel { get; set; }
        private CommandInvoker Invoker { get; set; }
        private TeacherCounter teacherCounter;

        private static IState _state;

        public Game()
        {
            Difficulty difficulty = ChooseDifficulty();

            Factory = difficulty switch
            {
                Difficulty.Easy => new EasyDifficultyFactory(),
                Difficulty.Medium => new MediumDifficultyFactory(),
                Difficulty.Hard => new HardDifficultyFactory(),
                _ => throw new ArgumentException("Invalid difficulty chosen"),
            };
            Player = Character.Instance;

            SetParametersAsDifficulty();

            AllUnits = new List<IMapObject>();

            Invoker = new CommandInvoker();
        }

        public void StartGame()
        {
            Player.Show();
            MainWindow.ShowStats(Player.Statistics);
            MainWindow.ShowPortrait(Player.Portrait);
            Teacher = Factory.CreateTeacherUnit();
            MainWindow.GetMainWindowController().PreviewKeyDown += CheckCollisions;
            MainWindow.GetMainWindowController().PreviewKeyDown += CheckIfLevelCompleted;
            MainWindow.GetMainWindowController().ExecuteCommand.Click += InvokerExecute;

            int TeacherCount = 1, RestCount = 6, DeadlineCount = 5;
            teacherCounter = new(TeacherCount);
            teacherCounter.Subscribe(this);

            BaseLevel level = new BaseLevel(AllUnits);
            level = new TeacherDecorator(level, AllUnits ,spawnTeacherUnits(TeacherCount));
            level = new RestDecorator(level, AllUnits, spawnRestUnits(RestCount));
            level = new DeadlineDecorator(level, AllUnits, spawnDeadlineUnits(DeadlineCount));
            currentLevel = level;

            level.Show();
        }

        private List<TeacherUnit> spawnTeacherUnits(int number)
        {
            List<TeacherUnit> units = [];
            for (int i = 0; i < number; i++)
            {
                units.Add(Teacher.Clone());
                //units[i].Show();
            }

            return units;
        }

        public void KillRandomTeacherUnit()
        {
            var Random = new Random();
            IMapObject t;

            do
            {
                t = AllUnits[Random.Next(AllUnits.Count)];
                if(t is TeacherUnit)
                {
                    t.Destroy();
                    AllUnits.Remove(t);
                }
            }
            while (t is not TeacherUnit);
        }

        private List<Rest> spawnRestUnits(int number)
        {
            List<Rest> units = [];
            for (int i = 0; i < number; i++)
            {
                units.Add(new Rest());
                //units[i].Show();
            }

            return units;
        }

        private List<IMapObject> spawnDeadlineUnits(int number)
        {
            List<IMapObject> units = [];
            for (int i = 0; i < number; i++)
            {
                units.Add(new DeadlineProxy(new Deadline(), "log.txt"));
            }

            return units;
        }

        public static void GameOver()
        {
            //MessageBox.Show("Woopsie, game over...");
            //MainWindow.GetMainWindowController();

            _state.GameOver();
            Application.Current.Shutdown();
        }

        public static void TransitionTo(IState state)
        {
            _state = state;
        }

        private static Difficulty ChooseDifficulty()
        {
            var win = new ChoosingDifficultyWindow();
            win.ShowDialog();

            return win.chosenDifficulty;
        }

        private void SetParametersAsDifficulty()
        {
            SetPortrait(Factory.CreateUserPortrait());
            SetStats(Factory.CreateUserStats());
            SetClonableUnit(Factory.CreateTeacherUnit());
        }

        private void SetPortrait(UserPortrait up)
        {
            Player.Portrait = up;
        }

        private void SetStats(UserStats us)
        {
            Player.Statistics = us;
        }

        private void SetClonableUnit(TeacherUnit tu)
        {
            Teacher = tu;
        }

        private void CheckCollisions(object sender, KeyEventArgs e)
        {
            foreach (var unit in AllUnits)
            {
                if (Player.CheckCollision(unit))
                {
                    if (unit is Rest)
                        Player.RestsInARow += 1;
                    else
                        Player.RestsInARow = 0;

                    if (unit is TeacherUnit)
                        teacherCounter.SubstractTeacher();
                    
                    unit.Hide();
                    AllUnits.Remove(unit);
                    break;
                }
            }
        }

        private void CheckIfLevelCompleted(object sender, KeyEventArgs e)
        {
            foreach(var unit in AllUnits)
            {
                if (unit is TeacherUnit) return;
            }

            //TODO Handle win situation: proceed to the clickable boss
        }

        private void InvokerExecute(object sender, EventArgs e)
        {
            var mw = MainWindow.GetMainWindowController();

            if(mw.MarchukHelp.IsChecked == true)
            {
                Invoker.AddCommand(new RomanMarchukHelp(Player));
            }
            if(mw.PovtorkaReminder.IsChecked == true)
            {
                Invoker.AddCommand(new PovtorkaReminderCommand(Player));
                Invoker.AddCommand(new KillTeacherUnitCommand(this));
            }

            Invoker.Execute();
            MainWindow.ShowStats(Player.Statistics);

        }

        public void Notify()
        {
            MainWindow.GetMainWindowController().mainGameCanvas.Children.Clear();

            MainWindow.GetMainWindowController().PreviewKeyDown -= CheckCollisions;
            MainWindow.GetMainWindowController().ExecuteCommand.Click -= InvokerExecute;

            ILevel level = new BossLevel(new TarasOrestovych());
            level.Show();
        }
    }
}
