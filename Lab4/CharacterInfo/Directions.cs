﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.CharacterInfo
{
    public enum Direction
    {
        Left,
        Up,
        Right,
        Down
    }
}
