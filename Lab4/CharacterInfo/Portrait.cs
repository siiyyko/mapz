﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.CharacterInfo
{
    public abstract class UserPortrait
    {
        public abstract string GetImagePath();
    }

    public class EasyUserPortrait : UserPortrait
    {
        public override string GetImagePath()
        {
            return "easy_portrait_path";
        }
    }

    public class MediumUserPortrait : UserPortrait
    {
        public override string GetImagePath()
        {
            return "medium_portrait_path";
        }
    }

    public class HardUserPortrait : UserPortrait
    {
        public override string GetImagePath()
        {
            return "../resources/portraits/portraitHard.jpg";
        }
    }
}
