﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.CharacterInfo
{
    public abstract class UserStats
    {
        public int Intellect { get; set; }
        public int Humor { get; set; }
        public int Slayness { get; set; }
        public int TimeManagement { get; set; }
        public int Bonds { get; set; }
    }

    public class EasyUserStats : UserStats
    {
        public EasyUserStats()
        {
            Intellect = 10;
            Humor = 10;
            Slayness = 10;
            TimeManagement = 10;
            Bonds = 10;
        }
    }

    public class MediumUserStats : UserStats
    {
        public MediumUserStats()
        {
            Intellect = 7;
            Humor = 7;
            Slayness = 7;
            TimeManagement = 7;
            Bonds = 7;
        }
    }

    public class HardUserStats : UserStats
    {
        public HardUserStats()
        {
            Intellect = 5;
            Humor = 5;
            Slayness = 5;
            TimeManagement = 5;
            Bonds = 5;
        }
    }
}
