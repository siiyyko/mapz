﻿using Lab4.MapObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Utilities
{
    public class CommandInvoker
    {
        private readonly List<ICommand> _commands;

        public CommandInvoker()
        {
            _commands = [];
        }

        public void AddCommand(ICommand command)
        {
            _commands.Add(command);
        }

        public void Execute()
        {
            foreach(var command in _commands)
            {
                command.Execute();
            }
            _commands.Clear();
        }
    }

    public class RomanMarchukHelp(Character character) : ICommand
    {
        private readonly Character _character = character;

        public void Execute()
        {
            _character.ApplyRomanMarchukHelp();
        }
    }

    public class PovtorkaReminderCommand(Character character) : ICommand
    {
        private readonly Character _character = character;

        public void Execute()
        {
            _character.ApplyPovtorkaReminder();
        }
    }

    public class KillTeacherUnitCommand(Game game) : ICommand
    {
        private readonly Game _game = game;

        public void Execute()
        {
            _game.KillRandomTeacherUnit();
        }
    }
}
