﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.CharacterInfo;
using Lab4.MapObjects;

namespace Lab4.Utilities
{

    public enum Difficulty
    {
        Easy,
        Medium,
        Hard
    }

    public interface IUserPortraitFactory
    {
        UserPortrait CreateUserPortrait();
    }

    public interface IUserStatsFactory
    {
        UserStats CreateUserStats();
    }

    public interface ITeacherFactory
    {
        TeacherUnit CreateTeacherUnit();
    }

    public interface IDifficultyFactory : IUserPortraitFactory, IUserStatsFactory, ITeacherFactory { }

    public class EasyDifficultyFactory : IDifficultyFactory
    {
        public UserPortrait CreateUserPortrait()
        {
            return new EasyUserPortrait();
        }

        public UserStats CreateUserStats()
        {
            return new EasyUserStats();
        }

        public TeacherUnit CreateTeacherUnit()
        {
            return new EasyTeacherUnit();
        }
    }

    public class MediumDifficultyFactory : IDifficultyFactory
    {
        public UserPortrait CreateUserPortrait()
        {
            return new MediumUserPortrait();
        }

        public UserStats CreateUserStats()
        {
            return new MediumUserStats();
        }

        public TeacherUnit CreateTeacherUnit()
        {
            return new MediumTeacherUnit();
        }
    }

    public class HardDifficultyFactory : IDifficultyFactory
    {
        public UserPortrait CreateUserPortrait()
        {
            return new HardUserPortrait();
        }

        public UserStats CreateUserStats()
        {
            return new HardUserStats();
        }

        public TeacherUnit CreateTeacherUnit()
        {
            return new HardTeacherUnit();
        }
    }

}
