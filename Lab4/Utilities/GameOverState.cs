﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab4.Utilities
{
    public abstract class IState
    {
        public abstract void GameOver();
    }

    public class LowHpGameOver : IState
    {
        public override void GameOver()
        {
            MessageBox.Show("КІНЕЦЬ.\nВи одразу розуміли, що робити 9 з ОС за один вечір маючи поруч з собою лише найліпшого друга і Театр пива Правда " +
                "- це недостатньо хороша ідея, але самі наступили на ті граблі, які чемно собі підготували ");
        }
    }

    public class TooMuchRestGameOver : IState
    {
        public override void GameOver()
        {
            MessageBox.Show("КІНЕЦЬ.\nВи не О-Rest, щоби так багато відпочивати... А відпочинок і каламбури у вас, схоже, виходять краще, ніж навчання на ПЗ. " +
                "Задумайтеся;)");

        }
    }

    public class DeadlineTouchedGameOver : IState
    {
        public override void GameOver()
        {
            MessageBox.Show("КІНЕЦЬ.\nНаближатись до дедлайну було помилкою. ");

        }
    }
}
