﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Utilities
{
    public class TeacherCounter(int teachers)
    {
        List<ISubscriber> subscribers = [];
        public int TeachersCount { get; private set; } = teachers;

        public void Subscribe(ISubscriber sub)
        {
            if(!subscribers.Contains(sub)) subscribers.Add(sub);
        }

        public void Unsubscribe(ISubscriber sub)
        {
            subscribers.Remove(sub);
        }

        public void SubstractTeacher()
        {
            TeachersCount -= 1;
            if (TeachersCount == 0) NotifySubscribers();
        }

        public void NotifySubscribers()
        {
            foreach(ISubscriber subscriber in subscribers)
            {
                subscriber.Notify();
            }
        }
    }
}
