using Lab_3;
using Lab3;
using Lab3.Difficulties;
using Lab3.Subjects;
using NuGet.Frameworks;
using System.ComponentModel.DataAnnotations;

namespace Lab3_UnitTest
{

    public class ClassTester
    {
        [Fact]
        public void TestSelect()
        {
            var expected = new List<double>()
            {
                100, 49, 96, 51, 64, 100, 0, 34.14, 81, 57.5
            };

            var actual = MockingClass.students.Select(stud => stud.Progress).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestCount()
        {
            var expected = 12;

            var actual = MockingClass.works.Count();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestWhere()
        {
            var expected = new List<string>()
            {
                "Nataliia Kaminska", "Khrystyna Hrytsai", "Viktor Shamatiienko"
            };

            var actual = MockingClass.students.Where(stud => stud.Name.Length > 16).Select(stud => stud.Name).Distinct().ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestOrderBy()
        {
            var expected = MockingClass.works[4];

            var actual = MockingClass.works.OrderBy(supervisor => supervisor.Difficulty).ThenBy(supervisor => supervisor.SupervisorName).Distinct().ToList();

            Assert.Equal(expected, actual[0]);
        }

        [Fact]
        public void TestFirst()
        {
            var expected = "Volodymyr Zhyla";

            var actual = MockingClass.students.Where(stud => stud.Progress < 50).Select(stud => stud.Name).First();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestMinBy()
        {
            var expected = "Viktor Shamatiienko";

            var actual = MockingClass.students.MinBy(stud => stud.Progress);

            Assert.Equal(expected, actual.Name);
        }

        [Fact]
        public void TestDictionary()
        {
            var actual = MockingClass.works.Where(work => work.Subject == Subject.SoftwareModelling)
                .ToDictionary(supervisor => supervisor.SupervisorName, supervisor => supervisor.Difficulty);

            var expected = new Dictionary<string, Difficulty>()
            {
                {"Pavlo Serdiuk",  Difficulty.Thoughtful},
                {"Mykola Milkovych", Difficulty.ItsOver},
                {"Yevhenia Levus", Difficulty.Intimidating}
            };

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void TestComparer()
        {
            var expected = MockingClass.students.OrderBy(stud => stud.Progress).ToList();

            var actual = MockingClass.students.OrderBy(stud => stud, new StudentsComparer()).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestAnonymous()
        {
            var expected = new
            {
                subject = Subject.DataBases,
                difficulty = Difficulty.EasyPeasy,
                name = "Svitlana Yatsyshyn"
            };

            var actual = MockingClass.works.Where(work => work.Difficulty == Difficulty.EasyPeasy && work.Subject == Subject.DataBases).Last();

            Assert.Equal(actual.SupervisorName, expected.name);
            Assert.Equal(actual.Difficulty, expected.difficulty);
            Assert.Equal(actual.Subject, expected.subject);
        }

        [Fact]
        public void TestGroupByFindRepetitions()
        {
            var expected = new List<string> { "Tetiana Koroteieva", "Yevhenia Levus" };

            var actual = MockingClass.works.GroupBy(w => w.SupervisorName)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key)
                .ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestGroupBySort()
        {
            var expected = MockingClass.students[2];

            var actual = MockingClass.students.GroupBy(s => s.CourseWorkSubject)
                .Select(g => g.OrderBy(s => s.Progress))
                .Select(g => g.ToArray())
                .ToArray();

            Assert.Equal(expected, actual[1][1]);
        }

        [Fact]
        public void TestToArray()
        {
            string[] expected =
            {
                "Oleksii Blyzniuk",
                "Nataliia Kaminska",
                "Mykyta Zakharov",
                "Khrystyna Hrytsai",
                "Kateryna Doskach"
            };

            var actual = MockingClass.students.Where(s => s.Progress > 50)
                .Select(s => s.Name)
                .Distinct()
                .ToArray();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestStudentCourseWorkStudy()
        {
            var expected = 5;

            MockingClass.students[6].studyOnCourseWork(2);
            var actual = MockingClass.students[6].Progress;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void TestEdgeCourseWorkStudy()
        {
            var expected = 100;

            MockingClass.students[0].studyOnCourseWork(2);
            var actual = MockingClass.students[0].Progress;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void TestStudentEdgeInitialization()
        {
            var expectedCompleted = true;
            var expectedProgress = 100;

            var actual = new Student("John Doe", Subject.SoftwareModelling, 100);

            Assert.Equal(expectedCompleted, actual.IsCompleted);
            Assert.Equal(expectedProgress, actual.Progress);
        }

        [Fact]
        public void TestStudentExtensionMethod()
        {
            var expected = 51.0;

            var actual = MockingClass.students[1].leftToComplete();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestTake()
        {
            double[] expected =
            {
                0, 34.14, 49
            };

            var actual = MockingClass.students.OrderBy(s => s.Progress)
                .Take(3)
                .Select(s => s.Progress)
                .ToArray();

            Assert.Equal(actual, expected);
        }
    }
}