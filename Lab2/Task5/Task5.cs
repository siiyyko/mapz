﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task5
{

    [Flags]
    internal enum WeekDays
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 4,
        Thursday = 8,
        Friday = 16,
        Saturday = 32,
        Sunday = 64,
        MiddleOfNowhen = Sunday << 2,
    }

    internal class Example
    {
        public Example() { }

        public void ShowEnumExamples()
        {
            WeekDays StudyingDays = WeekDays.Monday | WeekDays.Wednesday | WeekDays.Friday; // adding values to the StudyingDays

            if((WeekDays.Thursday & StudyingDays) == WeekDays.Thursday) // whether Thursday is in StudyingDays
            {
                Console.WriteLine("I study on Thursdays");
            }
            else
            {
                Console.WriteLine("I don't study on Thursdays");
            }

            WeekDays What = WeekDays.MiddleOfNowhen;
            Console.WriteLine("Ok, what day it is...? It's " + What + ", or " + (int)What);

            StudyingDays ^= WeekDays.Friday; // deleting a value from StudyingDays
            if ((WeekDays.Friday & StudyingDays) != WeekDays.Friday) // whether Friday is still in StudyingDays
            {
                Console.WriteLine("I no longer study on Fridays");
            }

        }
    }
}
