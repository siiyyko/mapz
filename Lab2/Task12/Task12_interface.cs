﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task11
{
    internal struct BaseInterfaceStruct : IBase
    {
        long a;
        public BaseInterfaceStruct()
        {
            a = 0;
        }

        void IBase.Execute()
        {
            a += 1;
        }
    }
}
