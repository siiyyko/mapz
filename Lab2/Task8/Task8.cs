﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task8
{
    internal class Irony
    {
        public int Deepness;
        public static int someField;
        public int oneMoreField = 2;
        static Irony()
        {
            Console.WriteLine("Static Base constructor happened");
            someField++;
        }

        public Irony(int Deepness)
        {
            this.Deepness = Deepness;
            Console.WriteLine("Usual Base constructor happened");
        }

    }

    internal class PostIrony : Irony
    {
        public int Seriousness;
        public static int anotherDerivedField;

        public PostIrony(int Deepness, int Seriousness) : base(Deepness)
        {
            this.Seriousness = Seriousness;
            Console.WriteLine("Usual Derived constructor happened");
        }

        static PostIrony()
        {
            Console.WriteLine("Static Derived constructor happened");
        }

    }
}
