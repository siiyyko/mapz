﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task9
{
    internal class OutRefTest
    {
        public OutRefTest() { }

        public void CalculateArraySum(int[] arr, out int sum)
        {
            sum = 0;
            foreach(int num in arr)
            {
                sum += num;
            }
        }

        public void Swap(ref int number1, ref int number2)
        {
            int temp = number2;
            number2 = number1;
            number1 = temp;
        }

        public void Swap(int number1, int number2)
        {
            int temp = number2;
            number2 = number1;
            number1 = temp;
        }

        public void Swap(string str1, string str2)
        {
            string temp = str2;
            str2 = str1;
            str1 = temp;
        }

        public void Swap(ref string str1, ref string str2)
        {
            string temp = str2;
            str2 = str1;
            str1 = temp;
        }
    }
}
