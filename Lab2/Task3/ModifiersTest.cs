﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task3
{
    class ClassModifierTest
    {
        int testField;
        void testMethod() { }
        int testProperty
        {
            get { return testField; }
            set { testField = value; }
        }

        ClassModifierTest()
        {
            
        }

        class SomeInnerClass
        {

        }

        struct SomeInnerStruct
        {

        }

        interface SomeInnerInterface
        {
            
        }
    }

    struct StructModifierTest
    {
        int testField;
        void testMethod() { }
        int testProperty
        {
            get { return testField; }
            set { testField = value; }
        }

        StructModifierTest(int some)
        {

        }

        class SomeInnerClass
        {

        }

        struct SomeInnerStruct
        {

        }

        interface SomeInnerInterface
        {

        }
    }

    interface InterfaceModifierTest
    {
        int testField { get; }
        void testMethod();
    }
}
