﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task11
{
    interface IBase
    {
        void Execute();
    }

    internal class BaseInterface : IBase
    {
        long a;
        public BaseInterface()
        {
            a = 0;
        }

        void IBase.Execute()
        {
            a += 1;
        }
    }
}
