﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Lab11
{
    public class Base
    {
        protected long a;
        public Base()
        {
            a = 0;
        }
        public virtual void Execute()
        {
            a += 1;
            Console.WriteLine("Base");
        }
    }

    public class DerivedOne : Base
    {
        public DerivedOne() { }

        public override void Execute()
        {
            a += 2;
            Console.WriteLine("DerivedOne");
        }
    }

    public class DerivedTwo : DerivedOne
    {
        public DerivedTwo() { }

        public override void Execute()
        {
            a += 3;
            Console.WriteLine("DerivedTwo");
        }
    }

    public class DerivedThree : DerivedTwo
    {
        public DerivedThree() { }

        public override void Execute()
        {
            a += 4;
        }
    }
}
