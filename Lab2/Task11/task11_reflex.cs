﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task11
{
    internal struct BaseReflex
    {
        public BaseReflex()
        {
            a = 0;
        }

        public long a;

        public void Execute()
        {
            a += 1;
        }
    }
}
