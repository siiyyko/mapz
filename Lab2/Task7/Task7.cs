﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task7
{
    internal class Flower
    {
        string Color;
        public Flower(string Color)
        {
            this.Color = Color;
        }

        public Flower()
        {
            this.Color = "Red";
        }

        public virtual void ShowPrettiness()
        {
            Console.WriteLine("I'm pretty! From parent class");
        }
    }

    internal class Rose : Flower
    {
        string Owner;
        uint height;
        public Rose(string color, string owner) : base(color)
        {
            this.Owner = owner;
            height = 0;
        }

        public Rose() : this("Whitish", "Siyko!") { }
        
        public override void ShowPrettiness()
        {
            Console.WriteLine("I'm pretty! From derived class");
        }

        public void Grow()
        {
            height += 1;
        }

        public void Grow(uint height)
        {
            this.height += height;
            base.ShowPrettiness();
            ShowPrettiness();
        }


    }
}
