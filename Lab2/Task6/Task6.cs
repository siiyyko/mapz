﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task6
{
    internal interface IWatchable
    {

    }

    internal interface ILaughable
    {

    }

    internal interface IAdulty
    {

    }

    internal class Cartoon : IWatchable
    {

    }

    internal class SouthPark: Cartoon, ILaughable, IAdulty
    {

    }
}
