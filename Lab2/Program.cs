﻿using Lab2.Lab11;
using Lab2.Task1;
using Lab2.Task11;
using Lab2.Task5;
using Lab2.Task7;
using Lab2.Task8;
using Lab2.Task9;
using System.Diagnostics;
using System.Reflection;

namespace Lab2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*task1*/
            //LaCosaNostra sicilian = new LaCosaNostra(100, "Sicilian mafia", "Inigo Quilez");

            //int privateMoney = sicilian.Money;
            //uint outerProtectedSize = sicilian.size;

            //string headName = sicilian.Head;
            //int respect = sicilian.Respect;
            //sicilian.areaControlled = 100;
            //sicilian.ammos = 100;

            //Example ex  = new Example();

            /*task3*/
            //ex.ShowEnumExamples();

            /*task6*/
            //Rose rose = new Rose();
            //rose.Grow(25);

            //PostIrony obj = new PostIrony(10, 100);

            /*task7*/
            //OutRefTest obj = new OutRefTest();

            //int[] arr = { 1, 2, 3, 4, 5, 6 };
            //obj.CalculateArraySum(arr, out int sum);
            //Console.WriteLine(sum);

            //int val1 = 5;
            //int val2 = 10;
            //obj.Swap(val1, val2);
            //Console.WriteLine(val1 + " " + val2);

            //obj.Swap(ref val1, ref val2);
            //Console.WriteLine(val1 + " " + val2);

            //string str1 = "Frisky";
            //string str2 = "Meow";

            //obj.Swap(str1, str2);
            //Console.WriteLine(str1 + " " + str2);

            //obj.Swap(ref str1, ref str2);
            //Console.WriteLine(str1 + " " + str2);

            /*task8*/
            //object myObj = 7;

            //int otherVar = (int)myObj;

            /*task9*/
            //double num1 = 7;

            //int temp = 5;
            //double num2 = (double)temp / 3.0;

            /*Individual Task*/

            Stopwatch sw = new Stopwatch();

            for(int j = 0; j < 5; ++j)
            {
                //Base virtobj = new DerivedThree();

                //sw.Restart();
                //for (int i = 0; i < 10e8; ++i)
                //{
                //    virtobj.Execute();
                //}
                //sw.Stop();

                //Console.WriteLine($"EXP{j} VIRTUAL: {sw.ElapsedMilliseconds}");

                BaseReflex reflexobj = new BaseReflex();
                Type type = reflexobj.GetType();
                MethodInfo methodInfo = type.GetMethod("Execute");

                sw.Restart();
                for (int i = 0; i < 10e8; ++i)
                {
                    methodInfo.Invoke(reflexobj, null);
                }
                sw.Stop();

                Console.WriteLine($"EXP{j} REFLEXION: {sw.ElapsedMilliseconds}");

                IBase interobj = new BaseInterface();
                sw.Restart();
                for (int i = 0; i < 10e8; ++i)
                {
                    interobj.Execute();
                }
                sw.Stop();

                Console.WriteLine($"EXP{j} INTERFACES: {sw.ElapsedMilliseconds}");

                Console.WriteLine("\n");
            }


        }
    }
}
