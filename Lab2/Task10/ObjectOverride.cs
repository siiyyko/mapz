﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task10
{
    internal class ObjectOverride : Object
    {
        public int someValue;

        public override string ToString()
        {
            return $"My value is {someValue}!";
        }

        public override bool Equals(object? obj)
        {
            Console.WriteLine("Additional text");
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return someValue * 5;
        }

        ~ObjectOverride()
        {
            Console.WriteLine("Destroyed...");
        }
    }
}
