﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task1
{
    internal interface ICriminals
    {
        void commitCrime();
        void hire(uint number);
    }
}
