﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task1
{
    internal class LaCosaNostra : Mafia
    {
        private int Money;
        private string LocalName;
        public string Head;
        internal int Respect;

        public LaCosaNostra(int money, string localName, string head)
        {
            this.Money = money;
            this.LocalName = localName;
            this.Head = head;
        }

        public override void commitCrime()
        {
            Console.WriteLine("Crime commited! Respect+, Money+");
        }

        public override void launder(int Sum)
        {
            Console.WriteLine($"{Sum}$ successfully laundered");
            Money += Sum;
        }

        public string GetLocalName => LocalName;
        public int GetMoneyCount => Money;
        public uint GetGroupSize => size;

        public int GetAmmos => ammos;
    }

}
