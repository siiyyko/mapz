﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2.Task1
{
    internal abstract class Mafia : ICriminals
    {
        protected uint size;
        private protected int ammos;
        protected internal int areaControlled;

        public abstract void commitCrime();
        public abstract void launder(int Sum);

        void ICriminals.hire(uint number)
        {
            Console.WriteLine($"{number} people were hired. Success!");
            size += number;
        }

    }
}
